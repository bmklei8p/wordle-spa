import React from 'react';


class Board extends React.Component {
    render() {
        return (
            <div className="container w-25">
                <table className="table table-bordered">
                    <thead>
                        <div className="d-flex justify-content-center"><h1>Wordle</h1></div>
                    </thead>
                    <br />
                    <br />
                    <tbody>
                        <tr className="d-flex justify-content-center">
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                        </tr>
                        <tr className="d-flex justify-content-center">
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                        </tr>
                        <tr className="d-flex justify-content-center">
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                        </tr>
                        <tr className="d-flex justify-content-center">
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                        </tr>
                        <tr className="d-flex justify-content-center">
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                        </tr>
                        <tr className="d-flex justify-content-center">
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                            <td><input className="tbl-cell" /></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
};



export default Board